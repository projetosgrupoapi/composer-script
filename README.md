# ComposerScript

*Classe helper para gerar scripts para Composer*

## Exemplo

Script para executar as rotinas de teste do PHPUnit.

Sintaxe: `$ composer [run-scripts] tests [purge-report] [suite <suite> [-- --debug]] [case <case> [-- --debug]]`

```php
<?php
use GrupoApi\Dev\ComposerTool;

class Tests extends ComposerTool
{
    public function execute(array $args)
    {
        if ($args) {
            while ($args) {
                $arg = array_shift($args);

                if ($arg === 'purge-report') {
                    $this->purgeReport();
                } elseif ($arg === 'case') {
                    $testcase = array_shift($args);
                    if ($args && $args[0] === '--debug') {
                        array_shift($args);
                        $this->runTestCase($testcase, true);
                    } else {
                        $this->runTestCase($testcase, false);
                    }
                } elseif ($arg === 'suite') {
                    $testsuite = array_shift($args);
                    if ($args && $args[0] === '--debug') {
                        array_shift($args);
                        $this->runTestSuite($testsuite, true);
                    } else {
                        $this->runTestSuite($testsuite, false);
                    }
                }
            }
        } else {
            $this->runAllTests();
        }
    }

    private function purgeReport()
    {
        $this->runProcess('rm -rf docs/code-coverage');
    }

    private function runTestCase($testcase, $debug)
    {
        $this->runProcessArgs(
            array_merge(
                array(
                    'phpunit',
                    '--configuration', 'tests/phpunit-nocc.xml',
                ),
                $debug ? array('--debug') : array(),
                array(
                    $testcase
                )
            )
        );
    }

    private function runTestSuite($testsuite, $debug)
    {
        $this->runProcessArgs(
            array_merge(
                array(
                    'phpunit',
                    '--configuration', 'tests/phpunit-nocc.xml',
                ),
                $debug ? array('--debug') : array(),
                array(
                    '--testsuite', $testsuite
                )
            )
        );
    }

    private function runAllTests()
    {
        $this->runProcessArgs(
            array(
                'phpunit',
                '--configuration', 'tests/phpunit.xml',
            )
        );
    }
}
```
