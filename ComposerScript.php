<?php
namespace GrupoApi\Dev;

use \RuntimeException;
use Composer\Composer;
use Composer\IO\ConsoleIO;
use Composer\Script\Event;
use Symfony\Component\Process\Process;

abstract class ComposerScript
{
    protected $composer;
    protected $io;

    public static function run(Event $event)
    {
        $composer = $event->getComposer();
        $io = $event->getIO();
        $args = $event->getArguments();

        $tool = new static($composer, $io);

        $tool->execute($args);
    }

    private function __construct(Composer $composer, ConsoleIO $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    abstract public function execute(array $args);

    protected function createCommandLine(array $args)
    {
        $args = array_map(
            function ($arg) {
                $escapedArg = escapeshellarg($arg);

                if ($escapedArg === "'$arg'" && strpos($arg, ' ') === false) {
                    return $arg;
                } else {
                    return $escapedArg;
                }
            },
            $args
        );

        return implode(' ', $args);
    }

    protected function runProcess($commandLine, $showOutput = true, &$output = null)
    {
        $process = new Process($commandLine);
        $process->setTimeout(0);

        $output = '';
        if ($showOutput) {
            $io = $this->io;
            if ($io->isDebug()) {
                $io->write('<info>$ '.$process->getCommandLine().'</info>');
            }
            $returnCode = $process->run(function ($type, $buffer) use($io, &$output) {
                $io->write($buffer, false);
                $output .= $buffer;
            });
        } else {
            $returnCode = $process->run(function ($type, $buffer) use(&$output) {
                $output .= $buffer;
            });
        }

        if (!$process->isSuccessful()) {
            throw new RuntimeException($process->getErrorOutput(), $returnCode);
        }

        return $this;
    }


    protected function runProcessArgs(array $args, $showOutput = true, &$output = null)
    {
        $commandLine = $this->createCommandLine($args);

        return $this->runProcess($commandLine, $showOutput, $output);
    }

    protected function runRemoteProcesses(
        array $commandLines,
        $host,
        $user = null,
        $path = null,
        $showOutput = true,
        &$output = null
    ) {

        if ($user === null) {
            $user = get_current_user();
        }

        $process = new Process(
            $this->createCommandLine(
                array(
                    'ssh',
                    '-T',
                    $user.'@'.$host
                )
            )
        );
        $process->setTimeout(0);

        $input = implode("\n", $commandLines);

        if ($path) {
            $input = $this->createCommandLine(
                array('cd', $path)
            )."\n".$input;
        }

        $process->setInput($input);

        if ($showOutput) {
            $io = $this->io;
            if ($io->isDebug()) {
                $io->write(
                    '<info>$ '.$process->getCommandLine().' < '.$input.'</info>'
                );
            }
            $process->run(function ($type, $buffer) use($io, &$output) {
                $io->write($buffer, false);
                $output .= $buffer;
            });
        } else {
            $process->run(function ($type, $buffer) use(&$output) {
                $output .= $buffer;
            });
        }

        return $this;
    }

    protected function openFile($path)
    {
        if (PHP_OS === 'Linux') {
            $this->runProcessArgs(
                array(
                    'xdg-open',
                    $path,
                    '&'
                ),
                false
            );
        } else {
            $this->runProcess($path, false);
        }
    }
}
